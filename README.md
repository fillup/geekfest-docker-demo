# Geekfest Docker Demo

## Getting Started
1. Download & Install Docker Toolbox: https://docker.com/docker-toolbox
1. Pull geekfest example: ```docker pull fillup/geekfest``` 
1. Run the image: ```docker run -d -P fillup/geekfest```
1. Check the process: ```docker ps``` and get the port, ex: ```32768```
1. Get the IP address for your Docker host VM: ```docker-machine ip default```
1. Open your browser and go to http://<docker-ip>:port

Link to slides: (http://bit.ly/geekfest-docker)
